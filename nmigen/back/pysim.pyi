from ..hdl.ast import (Signal, Delay as Delay,
                       Tick as Tick, Passive as Passive, Assign, Value)
from typing import Any, Iterable, Generator, Union, Callable, Optional

__all__ = ["Simulator", "Delay", "Tick", "Passive"]

ProcessCommand = Union[Delay, Tick, Passive, Assign, Value]
ProcessGenerator = Generator[ProcessCommand, Union[int, None], None]
Process = Union[ProcessGenerator, Callable[[], ProcessGenerator]]


class Simulator:
    def __init__(self,
                 fragment: Any,
                 vcd_file: Any = None,
                 gtkw_file: Any = None,
                 traces: Iterable[Signal] = ()):
        ...

    def __enter__(self) -> 'Simulator':
        ...

    def __exit__(self, *args: Any) -> None:
        ...

    def add_process(self, process: Process) -> None:
        ...

    def add_sync_process(self, process: Process) -> None:
        ...

    def add_clock(self,
                  period: float,
                  phase: Optional[float] = None,
                  domain: Any = "sync") -> None:
        ...

    def step(self, run_passive: bool = False) -> bool:
        ...

    def run(self) -> None:
        ...

    def run_until(self, deadline: float, run_passive: bool = False) -> bool:
        ...
