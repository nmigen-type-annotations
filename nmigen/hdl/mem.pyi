from typing import Optional, Iterable, List, Union
from .ast import Signal, Const

__all__ = ["Memory", "ReadPort", "WritePort"]


class ReadPort:
    memory: 'Memory'
    domain: str
    synchronous: bool
    transparent: bool
    addr: Signal
    data: Signal
    en: Union[Signal, Const]


class WritePort:
    memory: 'Memory'
    domain: str
    priority: int
    granularity: int
    addr: Signal
    data: Signal
    en: Signal


class Memory:
    width: int
    depth: int
    name: str

    def __init__(self,
                 width: int,
                 depth: int,
                 init: Optional[Iterable[int]] = None,
                 name: Optional[str] = None,
                 simulate: bool = True):
        ...

    # noinspection PyPropertyDefinition
    @property
    def init(self) -> List[int]:
        ...

    @init.setter
    def init(self, new_init: Optional[Iterable[int]]) -> None:
        ...

    def read_port(self,
                  domain: str = "sync",
                  synchronous: bool = True,
                  transparent: bool = True) -> ReadPort:
        ...

    def write_port(self,
                   domain: str = "sync",
                   priority: int = 0,
                   granularity: Optional[int] = None) -> WritePort:
        ...
