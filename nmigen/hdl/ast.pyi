from abc import ABCMeta, abstractmethod
from typing import (Union, Tuple, Any, Iterable, Optional, Mapping, overload,
                    List)
from collections.abc import MutableSequence

__all__ = [
    "Value", "Const", "C", "Mux", "Cat", "Repl", "Array", "ArrayProxy",
    "Signal", "ClockSignal", "ResetSignal", "Statement", "Assign", "Assert",
    "Assume", "Switch", "Delay", "Tick", "Passive"
]

ValueOrLiteral = Union[int, bool, 'Value']
ShapeResult = Tuple[int, bool]


class Value(metaclass=ABCMeta):
    @staticmethod
    def wrap(obj: ValueOrLiteral) -> 'Value':
        ...

    def __invert__(self) -> 'Value':
        ...

    def __neg__(self) -> 'Value':
        ...

    def __add__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __radd__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __sub__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __rsub__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __mul__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __rmul__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __mod__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __rmod__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __div__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __rdiv__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __lshift__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __rlshift__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __rshift__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __rrshift__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __and__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __rand__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __xor__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __rxor__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __or__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __ror__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __eq__(self, other: Any) -> Any:
        ...

    def __ne__(self, other: Any) -> Any:
        ...

    def __lt__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __le__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __gt__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __ge__(self, other: ValueOrLiteral) -> 'Value':
        ...

    def __len__(self) -> int:
        ...

    def __getitem__(self, key: Union[slice, int, str]) -> 'Value':
        ...

    def bool(self) -> 'Value':
        ...

    # noinspection PyMethodParameters
    def implies(premise: ValueOrLiteral,
                conclusion: ValueOrLiteral) -> 'Value':
        ...

    def part(self, offset: ValueOrLiteral, width: int) -> 'Value':
        ...

    def eq(self, value: ValueOrLiteral) -> 'Assign':
        ...

    @abstractmethod
    def shape(self) -> ShapeResult:
        ...


ShapeArgument = Union[int, Tuple[int, bool]]


class Const(Value):
    nbits: int
    signed: bool

    @staticmethod
    def normalize(value: int, shape: Tuple[int, bool]) -> int:
        ...

    def __init__(self, value: int,
                 shape: Optional[ShapeArgument] = None) -> None:
        ...

    def shape(self) -> ShapeResult:
        ...


C = Const


# noinspection PyPep8Naming
def Mux(sel: ValueOrLiteral,
        val1: ValueOrLiteral,
        val0: ValueOrLiteral) -> Value:
    ...


class Cat(Value):
    def __init__(self,
                 *args: Union[ValueOrLiteral,
                              Iterable[ValueOrLiteral]]) -> None:
        ...

    def shape(self) -> ShapeResult:
        ...


class Repl(Value):
    def __init__(self,
                 value: ValueOrLiteral,
                 count: int) -> None:
        ...

    def shape(self) -> ShapeResult:
        ...


# noinspection PyShadowingBuiltins
class Signal(Value):
    nbits: int
    signed: bool
    name: str
    reset: int
    reset_less: bool
    attrs: dict

    def __init__(self,
                 shape: Optional[ShapeArgument] = None,
                 name: Optional[str] = None,
                 reset: int = 0,
                 reset_less: bool = False,
                 min: Optional[int] = None, max: Optional[int] = None,
                 attrs: Optional[dict] = None,
                 decoder: Any = None) -> None:
        ...

    @classmethod
    def like(cls,
             other: ValueOrLiteral,
             name: Optional[str] = None) -> 'Signal':
        ...

    def shape(self) -> ShapeResult:
        ...


class ClockSignal(Value):
    def __init__(self, domain: str = "sync") -> None:
        ...

    def shape(self) -> ShapeResult:
        ...


class ResetSignal(Value):
    def __init__(self,
                 domain: str = "sync",
                 allow_reset_less: bool = False) -> None:
        ...

    def shape(self) -> ShapeResult:
        ...


StatementOrStatementList = Union[Iterable['Statement'], 'Statement']


class _StatementList(list):
    pass


class Statement:
    @staticmethod
    def wrap(obj: StatementOrStatementList) -> _StatementList:
        ...


class Assign(Statement):
    def __init__(self, lhs: ValueOrLiteral, rhs: ValueOrLiteral) -> None:
        ...


class Property(Statement):
    def __init__(self, test: ValueOrLiteral) -> None:
        ...


class Assert(Property):
    pass


class Assume(Property):
    pass


class Switch(Statement):
    def __init__(self,
                 test: ValueOrLiteral,
                 cases: Mapping[Union[bool, int, str],
                                StatementOrStatementList]) -> None:
        ...


class Delay(Statement):
    def __init__(self, interval: Optional[float]) -> None:
        ...


class Tick(Statement):
    def __init__(self, domain: Any = "sync"):
        ...


class Passive(Property):
    pass


class Array(MutableSequence):
    def __init__(self, iterable: Iterable = ()):
        ...

    @overload
    def __getitem__(self, index: int) -> Any:
        ...

    @overload
    def __getitem__(self, index: Value) -> 'ArrayProxy':
        ...

    def __len__(self) -> int:
        ...

    def __setitem__(self, index: int, value: Any) -> None:
        ...

    def __delitem__(self, index: int) -> None:
        ...

    def insert(self, index: int, value: Any) -> None:
        ...


class ArrayProxy(Value):
    def __init__(self, elems: Union[Array, List[Any]], index: ValueOrLiteral):
        ...

    def __getattr__(self, attr: Any) -> Any:
        ...

    def __getitem__(self, index: Any) -> Any:
        ...

    def shape(self) -> Tuple[int, bool]:
        ...
