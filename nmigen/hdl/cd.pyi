from .ast import Signal
from typing import Optional

__all__ = ["ClockDomain", "DomainError"]


class DomainError(Exception):
    pass


class ClockDomain:
    clk: Signal
    rst: Optional[Signal]

    def __init__(self,
                 name: Optional[str] = None,
                 reset_less: bool = False,
                 async_reset: bool = False):
        ...

    def rename(self, new_name: str) -> None:
        ...
