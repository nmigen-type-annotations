# need "a as a" to work around mypy bug
from .hdl.dsl import Module as Module
from .hdl.ast import (Value as Value,
                      Const as Const,
                      C as C,
                      Mux as Mux,
                      Cat as Cat,
                      Repl as Repl,
                      Signal as Signal,
                      ClockSignal as ClockSignal,
                      ResetSignal as ResetSignal)
from .hdl.cd import ClockDomain as ClockDomain
from .hdl.ir import (Fragment as Fragment,
                     Instance as Instance,
                     Elaboratable as Elaboratable)
from .hdl.mem import Memory as Memory
from .hdl.rec import Record as Record
